# What's this
This is a personal notebook of areas that need improvements and feature ideas.

## 4.5
- Persistent GUI options (autokey, etc)
- Playhead snapping: [#91973: Design: More frame snapping options](https://projects.blender.org/blender/blender/issues/91973)


## General TODOs
* better design for "Play in" feature [#122812: options "play in ..." of the timeline does not seem to work](https://projects.blender.org/blender/blender/issues/122812)
* Euler/Quats preference options

## Bugs to Fix
* for 4.5 [#97852: Bone weights dont normalize from assign automatic from bones](https://projects.blender.org/blender/blender/issues/97852) with a big refactor before

## Cleanups and Refactors
* `rna_FKeyframe_ctrlpoint_ui_set` use common function to set key with handles
* unify keyframe deletion functions
* cleanup anim_channels_edit.cc : eAnimFilter_Flags enum, and enum casts
* cleanup `ED_object_vgroup_calc_from_armature` to use proper types for enum and bool

## Keying in general
* Improve the keyframing python api. Expose all options, but also give an option to use the users settings.
* Visibility state inconsistent between Graph Editor and the Dope Sheet
* When hovering over properties, ignore certain keying flags
* When hovering over array properties, only key the index under the cursor

## Armatures
* per bone "draw in front" setting
* new feature to disable shading per bone

## Pose Sliders
* Mirroring should work with pose sliders [#119411: Pose Sliding Tools Dont work as expected when Mirror is On.](https://projects.blender.org/blender/blender/issues/119411)
* breakdowner not respecting NLA layers


## Copy pasting animation data
* copy between files
* copy/paste world space
* copy/paste animation data
* prototype: [#115675: WIP: world space copy/paste prototype](https://projects.blender.org/blender/blender/pulls/115675)


## Graph Editor
* Implement locked handles
* Locked curves draw with checker pattern or improve dashed drawing
* Fully convert to new snapping system
* Improve performance by batching draw calls (refer to `sequencer_quads_batch.hh`)
* Slider operators to extrapolate from the closest two keys when invoked from the keyed range. e.g. frame on 1 and 3, called on 5


## Motion Paths
* add velocity heat color mode

### Update dynamically to the viewports current camera.
This needs a depsgraph evaluation of the camera on every frame of the motion path.
Interesting testbed for onion skinning.

## Object Mode

* Breakdowner and Blend to Neighbor in object mode

## Onion Skinning
prototype: [#107641: WIP: Onion Skinning Prototype](https://projects.blender.org/blender/blender/pulls/107641)
* need to figure out how to evaluate a depsgraph on a different frame and store the result (cache invalidation)

## Grouping improvements
* https://blender.community/c/rightclickselect/kwqY/
* https://blender.community/c/rightclickselect/pRVm/
