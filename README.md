| Platform | Contact |
| - | - |
| blender.chat | [ChristophLendenfeld](https://blender.chat/direct/ChristophLendenfeld) |
| Email | chris.lenden@gmail.com |
| Mastodon | [VieleAnimations](https://mastodon.art/@vieleanimations)

# About

Starting off as an animator, I stumbled into the tech side of an animation pipeline.
At the time of writing this I work as a Pipeline TD and Blender Developer, both part time.

On the Blender side I am part of the [Animation & Rigging module](https://projects.blender.org/blender/blender/wiki/Module:%20Animation%20&%20Rigging).